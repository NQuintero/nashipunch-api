# nashipunch-api
### a NodeJS forum backend

# License

right now, there is no official license. assume that you can't reuse this code for other things, that you're willingly contributing to this project and that you're giving away your contributions and that nothing belongs to you etc.  
i'll figure out the best license for it later, at which point your contributions should gain the (more permissive) rights of that license.

# Running this project

Requirements:

* Node v9 or later
* MySQL server (legacy auth might be required)
* Redis
    * Windows: https://github.com/ServiceStack/redis-windows/raw/master/downloads/redis-latest.zip  
    Run `redis-server.exe` when running the API.
    * Linux: https://redis.io/topics/quickstart
    * OSX: https://medium.com/@djamaldg/install-use-redis-on-macos-sierra-432ab426640e

Recommended:

* yarn

Instructions:

1. Clone the repo
2. Navigate to `/nashipunch-api`
3. Run a `yarn` (or `npm install` if you promise you won't push npm-related files to the repo)
4. Start up redis and your mysql server (in case you hadn't)
5. Run `yarn global add sequelize-cli` (or `npm install -g sequelize-cli`) to install the sequelize CLI
5. Run `sequelize db:create nashipunch_db` to create the database
6. Run `sequelize db:migrate` to create the tables
7. Run `yarn start` (or `npm start`) to fire up your server
8. Open `http://localhost:3000/thread` on your browser to test that the API is working

# Contributing to nashipunch-api

1. Fork this repository
2. Do work, push it to your repository
3. Open **your** repository's bitbucket page, hit the + button on the sidebar
4. `Create a pull request`
5. Target `nashipunch/nashipunch-api`, fill in the title and description fields, create the PR!
6. @ Inacio on Facepunch (if it still exists) or in the Favela Lounge Discord server and let me know you created a PR so i can give it a look

# Development procedure

1. When creating a task on trello, it should be estimated to be done in a maximum time of 24 hours.
2. When you have any technical questions, go to the Favela Lounge Discord.
3. When there is a disagreement about non-technical design elements, create a poll so the community can decide what option would be best.

# Feedback procedure

1. Comments, suggestions, additions and criticism* should be posted on the NotPunch thread in the developers category on facepunch, or in the Favela Lounge Discord.
2. Comments, suggestions, additions and criticism* are always welcome. thanks for your help!
3. Do not use the trello board for any comments, suggestions criticism. We think your feedback is useful and we will look into any issues, suggestions, or criticism you give. We will create a work-item based on your report when we have decided what to do with it.
4. If you ever think that we are making decisions that are not in sync with the expectations of the community, please create a poll so we can see if our decision is wrong. We will adapt the forum when the community desires it.

*Unless that criticism is about this project being made with javascript