const {
    NODE_ENV = 'development',
    JWT_SECRET = 'test_jwt_password',
    REDIS_PORT = 6379,
    REDIS_HOST = '',
    APP_PORT = 3000,
    POSTS_PER_PAGE = 20
 } = process.env

module.exports = {
    APP_PORT,
    NODE_ENV,
    JWT_SECRET,
    REDIS_PORT,
    REDIS_HOST,
}