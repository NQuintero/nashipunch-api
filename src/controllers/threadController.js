const httpStatus = require("http-status");
const knex = require("../services/knex").knex;

const datasource = require("../models/datasource");
const { POSTS_PER_PAGE } = require("../../config/server");

const composer = require("../helpers/queryComposer");
const Response = require("../helpers/responseStrategy");
const Pagination = require("../helpers/PaginationBuilder");

const redis = require("../services/redisClient");

const { Thread } = datasource().models;

exports.index = async (req, res) => {
  const scope = composer.scope(req, Thread);
  const options = composer.options(req, Thread.blockedFields);

  options.distinct = true;
  options.col = "Thread.id";

  try {
    result = await scope.findAndCountAll(options);

    const pagResult = new Pagination(options, req, result).build();
    redis.setex(composer.keyCache(req), 1, JSON.stringify(pagResult));

    Response.send(res, pagResult, options);
  } catch (error) {
    console.error(error);
    Response.send(res, error, options);
  }
};
exports.show = async (req, res) => {
  const options = composer.options(req, Thread.blockedFields);

  options.distinct = true;
  options.col = "Thread.id";
  options.parameters = [req.query.postPage, req.params.id];

  const scope = composer.scope(req, Thread, options);

  try {
    result = await scope.findOne(options);

    redis.setex(composer.keyCache(req), 1, JSON.stringify(result));

    Response.send(res, result, options);
  } catch (error) {
    console.error(error);
    Response.send(res, error, options);
  }
};
exports.store = async (req, res) => {
  const scope = composer.scope(req, Thread);

  try {
    const thread = await Thread.create({ ...req.body, user_id: req.user.id });

    const result = await scope.findOne({ where: { id: thread.id } });

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json(exception);
  }
};
exports.update = async (req, res) => {
  const scope = composer.scope(req, Thread);

  try {
    const thread = await Thread.findOne({ where: req.params });

    await thread.update(req.body);

    const result = await scope.findOne({ where: req.params });

    res.status(httpStatus.OK);
    res.json(result);
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.status(httpStatus.UNPROCESSABLE_ENTITY).send(err.message);
  }
};
exports.destroy = async (req, res) => {
  Thread.destroy({ where: req.params })
    .then(() => res.sendStatus(httpStatus.NO_CONTENT))
    .catch(() => res.status(httpStatus.UNPROCESSABLE_ENTITY));
};
exports.count = (req, res) => {
  const options = composer.onlyQuery(req);

  Thread.count(options)
    .then(result => {
      res.status(httpStatus.OK);
      res.json(result);
    })
    .catch(err => {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json(err.message);
    });
};
exports.withPostsAndCount = async (req, res) => {
  console.log(req.params);

  try {
    const count = await knex
      .count()
      .from("posts as po")
      .where("po.thread_id", req.params.id);

    const PAGE_POSTS = count["count(*)"];

    const PAGE_HAS_POSTS = PAGE_POSTS - 20 * (req.params.page - 1) > 0;

    if (PAGE_HAS_POSTS) {
      console.log("START");
      const query = await knex
        .from("posts as po")
        .select(
          "th.id as threadId",
          "th.title as threadTitle",
          "th.icon_id as threadIconId",
          "th.user_id as threadUserId",
          "th.subforum_id as threadSubforumId",
          "th.created_at as threadCreatedAt",
          "th.updated_at as threadUpdatedAt",
          "po.id as postId",
          "po.content as postContent",
          "po.user_id as postUserId",
          "po.created_at as postCreatedAt",
          "po.updated_at as postUpdatedAt",
          "po.thread_id as postThreadId",
          "us.username as userUsername",
          "us.usergroup as userUsergroup",
          "us.avatar_url as userAvatar_url",
          "us.created_at as userCreated_at"
        )
        .leftJoin("threads as th", "po.thread_id", "th.id")
        .join("users as us", "us.id", "po.user_id")
        .where("th.id", req.params.id)
        .limit(20)
        .offset(req.params.page ? (req.params.page - 1) * 20 : 0);
      console.log("END");

      const queryResults = Array.isArray(query) ? query : [query];

      const posts = queryResults.map(item => {
        const {
          postId,
          postContent,
          postUserId,
          postCreatedAt,
          postUpdatedAt,
          postThreadId,
          userUsername,
          userUsergroup,
          userAvatar_url,
          userCreated_at
        } = item;

        return {
          id: postId,
          content: postContent,
          createdAt: postCreatedAt,
          updatedAt: postUpdatedAt,
          user: {
            id: postUserId,
            username: userUsername,
            usergroup: userUsergroup,
            avatar_url: userAvatar_url,
            createdAt: userCreated_at
          }
        };
      });

      const {
        threadId,
        threadTitle,
        threadIconId,
        threadUserId,
        threadSubforumId,
        threadCreatedAt,
        threadUpdatedAt
      } = queryResults[0];

      const user = await knex
      .from("users")
      .select("username", "usergroup")
      .where("id", threadUserId);

      const thread = {
        id: threadId,
        title: threadTitle,
        iconId: threadIconId,
        userId: threadUserId,
        subforumId: threadSubforumId,
        createdAt: threadCreatedAt,
        updatedAt: threadUpdatedAt,
        currentPage: req.params.page || 1,
        user
      };

      const response = {
        ...thread,
        totalPosts: PAGE_POSTS,
        posts
      };

      return Response.send(res, response);
    } else {
      console.log("Page has no posts. Return thread");

      const query = await knex
        .from("threads as th")
        .select(
          "th.id as threadId",
          "th.title as threadTitle",
          "th.icon_id as threadIconId",
          "th.user_id as threadUserId",
          "th.subforum_id as threadSubforumId",
          "th.created_at as threadCreatedAt",
          "th.updated_at as threadUpdatedAt"
        )
        .where("th.id", req.params.id);

      const {
        threadId,
        threadTitle,
        threadIconId,
        threadUserId,
        threadSubforumId,
        threadCreatedAt,
        threadUpdatedAt
      } = query;

      const thread = {
        id: threadId,
        title: threadTitle,
        iconId: threadIconId,
        userId: threadUserId,
        subforumId: threadSubforumId,
        createdAt: threadCreatedAt,
        updatedAt: threadUpdatedAt,
        currentPage: req.params.page || 1,
        totalPosts: PAGE_POSTS,
        posts: []
      };

      return Response.send(res, thread);
    }
  } catch (error) {
    console.error(error);
    Response.send(res, error);
  }
};
