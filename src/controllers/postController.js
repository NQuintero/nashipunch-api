const httpStatus = require("http-status");
const knex = require("../services/knex").knex;

const datasource = require("../models/datasource");

const composer = require("../helpers/queryComposer");
const Response = require("../helpers/responseStrategy");
const Pagination = require("../helpers/PaginationBuilder");

const redis = require("../services/redisClient");

const { Post } = datasource().models;

exports.index = async (req, res) => {
  const scope = composer.scope(req, Post);
  const options = composer.options(req, Post.blockedFields);

  options.distinct = true;
  options.col = "Post.id";

  try {
    result = await scope.findAndCountAll(options);

    const pagResult = new Pagination(options, req, result).build();
    redis.setex(composer.keyCache(req), 1, JSON.stringify(pagResult));

    Response.send(res, pagResult, options);
  } catch (error) {
    console.error(error);
    Response.send(res, error, options);
  }
};
exports.show = (req, res) => {
  const scope = composer.scope(req, Post);
  const options = composer.options(req, Post.blockedFields);

  scope
    .findOne(options)
    .then(result => {
      redis.setex(composer.keyCache(req), 1, JSON.stringify(result));

      Response.send(res, result, options);
    })
    .catch(err => {
      console.error(err);
      Response.send(res, err, options);
    });
};
exports.store = async (req, res) => {
  const scope = composer.scope(req, Post);

  try {
    const post = await Post.create({ ...req.body, user_id: req.user.id });

    const result = await scope.findOne({ where: { id: post.id } });

    knex("threads")
      .where({ id: req.body.thread_id })
      .update("updated_at", new Date())
      .then(rows => console.log(rows))
      .catch(err => console.log(err));

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    console.log(exception);

    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json(exception);
  }
};
exports.update = async (req, res) => {
  const scope = composer.scope(req, Post);

  try {
    const post = await Post.findOne({ where: req.params });
    await post.update(req.body);

    const result = await scope.findOne({ where: req.params });

    knex("threads")
      .where({ id: req.body.thread_id })
      .update("updated_at", new Date())
      .then(rows => console.log(rows))
      .catch(err => console.log(err));

    res.status(httpStatus.OK);
    res.json(result);
  } catch (err) {
    res.status(httpStatus.BAD_REQUEST);
    res.status(httpStatus.UNPROCESSABLE_ENTITY).send(err.message);
  }
};
exports.destroy = async (req, res) => {
  Post.destroy({ where: req.params })
    .then(() => res.sendStatus(httpStatus.NO_CONTENT))
    .catch(() => res.status(httpStatus.UNPROCESSABLE_ENTITY));
};
exports.count = (req, res) => {
  const options = composer.onlyQuery(req);

  Post.count(options)
    .then(result => {
      res.status(httpStatus.OK);
      res.json(result);
    })
    .catch(err => {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json(err.message);
    });
};
