"use strict";
module.exports = (sequelize, DataTypes) => {
  const Thread = sequelize.define(
    "Thread",
    {
      title: DataTypes.STRING,
      icon_id: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
      subforum_id: DataTypes.INTEGER,
      createdAt: { type: DataTypes.DATE, field: "created_at" },
      updatedAt: { type: DataTypes.DATE, field: "updated_at" }
    },
    {
      timestamps: true,
      scopes: {
        withPosts: function(postPage = 0) {
          const offset = postPage * 3 - 3;

          return {
            include: [
              {
                association: Thread.Posts,
                attributes: ["id", "content", "user_id", "created_at", "updated_at", "thread_id"],
                limit: 20,
                offset: offset >= 0 ? offset : 0
              },
              {
                association: Thread.User,
                attributes: ["username", "usergroup", "created_at", "avatar_url"]
              }
            ]
          };
        },
        countPosts: function(postPage, threadId) {
          return {
            model: sequelize.models.Post,
            attributes: [[sequelize.fn("COUNT", sequelize.col("posts.id")), "countTask"]]
          };
        }
      }
    }
  );
  Thread.associate = function(models) {
    // A user can have many posts
    Thread.Posts = Thread.hasMany(models.Post, { as: "posts", foreignKey: "thread_id" });
    Thread.User = Thread.belongsTo(models.User, { as: "user", foreignKey: "user_id" });
    Thread.Subforum = Thread.belongsTo(models.Subforum, {
      as: "subforum",
      foreignKey: "subforum_id"
    });
  };
  Thread.initScopes = () => {};
  return Thread;
};
