const { host, username, password, database } = require("../../config/config")["development"];

const knex = require("knex");

module.exports = {knex: knex({
  client: "mysql",
  connection: {
    host,
    user: username,
    password,
    database
  },
  postProcessResponse: (result, queryContext) => {
    if(result.length === 1){
      return JSON.parse(JSON.stringify(result[0]))
    }

    return JSON.parse(JSON.stringify(result))
  }
})};
